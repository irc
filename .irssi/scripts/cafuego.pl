use strict;
use vars qw($VERSION %IRSSI);

$VERSION = "1.00";
%IRSSI = (
	authors     => 'Cafuego',
	contact     => 'irssi\@cafuego.net',
	name   	    => 'cafuego',
	description => 'Filters BitchX quit messages.',
	license     => 'GPL',
	url         => '',
	changed     => '$Id: cafuego.pl,v 1.1 2002/10/09 07:11:58 cafuego Exp cafuego $ ',
);

my $bx_count = 0;

sub scan_bitchx {
  my ($server,$msg,$nick,$address,$target) = @_;
  if ($msg =~ /BitchX/ || $msg =~ /bitchx/ || $msg =~ /BX/ || $msg =~ /Bersirc/i) {
    Irssi::print("Stopped moron BitchX message from ".$nick." [".++$bx_count."]");
    Irssi::signal_stop();
  }
}

Irssi::signal_add("event away", "scan_bitchx");
Irssi::signal_add("event part", "scan_bitchx");
Irssi::signal_add("event quit", "scan_bitchx");
