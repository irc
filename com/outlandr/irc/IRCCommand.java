package com.outlandr.irc;

public class IRCCommand {

	private String command;
	private String data;

	public IRCCommand(String command, String data) {
		this.command = command;
		this.data = data;
	}

	public String getCmmand() {
		return command;
	}

	public String getData() {
		return data;
	}

}
