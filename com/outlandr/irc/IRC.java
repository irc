package com.outlandr.irc;
/*
*
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class IRC {
	
	private Socket socket;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private BufferedReader userIn = null;
	private MessageHandler serverMessageHandler;
	private MessageHandler userMessageHandler;
	private Object monitor = new Object();
	
	public IRC(String host, Integer port) {
		this();
		connect(host, port);
		initStreams();
		serverMessageHandler = new MessageHandler(in, monitor);
		userMessageHandler = new MessageHandler(userIn, monitor);
		serverMessageHandler.start();
		userMessageHandler.start();
	}
	
	public IRC() {
		socket = new Socket();
	}

	private void initStreams() {
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			userIn = new BufferedReader(
				       new InputStreamReader(System.in));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connect(String host, Integer port) {
		SocketAddress socketAddress = new InetSocketAddress(host, port);
		try {
			socket.connect(socketAddress);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Generic SendCommand method.
	public void sendCommand(String command) {
		out.println(command);
	}
	
	
	public void nick(String nick) {
		sendCommand("NICK " + nick);
	}
	public void pong(String pingMessage) {
		String substring = pingMessage.substring(pingMessage.indexOf(':'), pingMessage.length());
		sendCommand("PONG " + substring);
	}
	
	public IRCCommand readOutput ()  {
		String message = serverMessageHandler.getMessage();
		
		if (message != null) {
			System.out.println(message);
			if (message.contains("PING")) {
				return new IRCCommand("PING", message);
			}
		}
		return null;
	}
	
	public void mainLoop() {
		
		nick("guest");
//		readOutput();
		
//		boolean done = false;
		
		
		String clientCommand = "USER guest www.google.com localhost :Guest";
		sendCommand(clientCommand);
		
		while(true) {
			IRCCommand command = null;
			command = readOutput();
			//			IRCCommand command = parseCommand();
			if (command != null && "PING".equals(command.getCmmand())) {
				pong(command.getData());
			}
			
			clientCommand = getClientCommand();
						
			if (clientCommand != null) {
				sendCommand(clientCommand);
			}
			if (!serverMessageHandler.hasMessages() && !userMessageHandler.hasMessages()) {
				try {
					synchronized (monitor) {
						monitor.wait();
					}
				} catch (InterruptedException e) {
					// TODO log error
					e.printStackTrace();
				}
			}
		}
	}
	

	private String getClientCommand() {
		String message = userMessageHandler.getMessage();
		return message;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		String SERVER = "irc.absurd-irc.net";
		String SERVER = "irc.backwerds.net";
		
		IRC irc = new IRC(SERVER, 6667);
		irc.mainLoop();
		

	}

}
