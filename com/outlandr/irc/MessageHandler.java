package com.outlandr.irc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;

public class MessageHandler extends Thread {

	private BufferedReader in;
	private LinkedList<String> messages = new LinkedList<String>();
	private Object monitor;
	
	private MessageHandler() {};

	public MessageHandler(BufferedReader in, Object monitor) {
		this.in = in;
		this.monitor = monitor;
	}
	public String getMessage() {
		if (hasMessages()) {
			return messages.removeFirst(); 
		}
		return null;
	}

	@Override
	public void run() {
		String data;
		
		try {
			while ((data = in.readLine()) != null) {
				messages.add(data);
				synchronized (monitor) {
					monitor.notifyAll();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean hasMessages() {
		return !messages.isEmpty();
	}

}
